package com.classpath.ordersapi.service;

import com.classpath.ordersapi.event.EventType;
import com.classpath.ordersapi.event.OrderEvent;
import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;
import io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

@Service
@RequiredArgsConstructor
@Slf4j
public class OrderService {

    private final OrderRepository orderRepository;
    private final RestTemplate restTemplate;
    private final Source source;

    @CircuitBreaker(name="inventoryservice", fallbackMethod = "fallback")
    public Order saveOrder(Order order) {
        //connect to inventory microservice
        //make a REST call to the post method
        Order savedOrder = this.orderRepository.save(order);
        //Instead of making a rest call, we will post the order event to the topic
        //producer api to publish the order event to the broker
        final OrderEvent orderEvent = new OrderEvent(savedOrder, EventType.ORDER_PENDING, LocalDateTime.now());
        this.source.output().send(MessageBuilder.withPayload(orderEvent).build());
        /*final ResponseEntity<Integer> responseEntity = this.restTemplate.postForEntity("http://inventory-microservice/api/inventory", null, Integer.class);
          log.info(" Response from inventory microservice :: {}", responseEntity.getBody());*/


        return savedOrder;
    }

    private Order fallback(Exception exception){
        log.error("Exception while connection to inventory microservice :: {}", exception.getMessage());
        return Order.builder().orderId(1111L).price(45000).date(LocalDate.now()).build();
    }

    public Map<String, Object> fetchAllOrders(int page, int size, String property) {
        PageRequest pageRequest = PageRequest.of(page, size, Sort.by(property));
        final Page<Order> pageResponse = this.orderRepository.findAll(pageRequest);
        final long totalRecords = pageResponse.getTotalElements();
        final int pages = pageResponse.getTotalPages();
        final List<Order> data = pageResponse.getContent();

        //Create a linkedHashMap
        Map<String, Object> resultMap = new LinkedHashMap<>();
        resultMap.put("total-records", totalRecords);
        resultMap.put("pages", pages);
        resultMap.put("data", data);
        return resultMap;
    }

    public Order fetchOrderById(long orderId) {
        return this.orderRepository.findById(orderId).orElseThrow(() -> new IllegalArgumentException("Invalid order id passed"));
    }

    public Order updateOrder(long orderId, Order order) {
        Order savedOrder = this.fetchOrderById(orderId);
        savedOrder.setPrice(order.getPrice());
        savedOrder.setDate(order.getDate());
        savedOrder.setCustomer(order.getCustomer());
        return this.orderRepository.save(savedOrder);
    }

    public void deleteOrderById(long orderId) {
        this.orderRepository.deleteById(orderId);
    }

    public Set<Order> fetchAllOrdersByPrice(double min, double max) {
        return this.orderRepository.findByPriceBetween(min, max);
    }
}