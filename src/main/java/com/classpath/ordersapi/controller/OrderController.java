package com.classpath.ordersapi.controller;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.service.OrderService;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.servers.Server;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Map;
import java.util.Set;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;

@RestController
@RequestMapping("api/orders")
@CrossOrigin(origins = "*")
@Slf4j
@OpenAPIDefinition(
        info = @Info(title = "Orders API", contact = @Contact(name = "dev", email = "developer@gmail.com")),
        servers = {@Server(url = "http://localhost:8222"), @Server(url = "http://localhost:8333")}
)
public class OrderController {

    private final OrderService orderService;

    public OrderController(OrderService orderService) {
        this.orderService = orderService;
    }

    @PostMapping
    @ResponseStatus(CREATED)
    @Operation(method = "fetch all orders", responses = {
            @ApiResponse(responseCode = "201", description = "saving the order"),
            @ApiResponse(responseCode = "400", description = "invalid order schema passed"),
            @ApiResponse(responseCode = "404", description = "invalid url passed"),
    })

    public Order saveOrder(
            @io.swagger.v3.oas.annotations.parameters.RequestBody(required = true, description = "order to be persisted")
            @Valid
            @RequestBody Order order) {
        return this.orderService.saveOrder(order);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(method = "fetch all orders", responses = {
            @ApiResponse(responseCode = "200", description = "fetching all the orders"),
            @ApiResponse(responseCode = "404", description = "invalid url passed"),
    })
    public Map<String, Object> fetchAll(
            @RequestParam(name = "page", required = false, defaultValue = "0") int page,
            @RequestParam(name = "size", required = false, defaultValue = "10") int size,
            @RequestParam(name = "attr", required = false, defaultValue = "customerName") String property
    ) {
        return this.orderService.fetchAllOrders(page, size, property);
    }

    @GetMapping(value = "/price", produces = MediaType.APPLICATION_JSON_VALUE)
    @Operation(method = "fetch all orders matching the price range", responses = {
            @ApiResponse(responseCode = "200", description = "fetching all the orders matching the price range"),
            @ApiResponse(responseCode = "404", description = "invalid url passed"),
    })

    public Set<Order> fetchOrdersByPrice(
            @RequestParam(name = "min", required = false, defaultValue = "20000") double min,
            @RequestParam(name = "max", required = false, defaultValue = "50000") double max) {
        return this.orderService.fetchAllOrdersByPrice(min, max);
    }

    @GetMapping(value = "/{orderId}", produces = {MediaType.APPLICATION_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @Operation(method = "fetch all orders", responses = {
            @ApiResponse(responseCode = "200", description = "fetching order matching the order id passed"),
            @ApiResponse(responseCode = "404", description = "invalid url passed"),
    })
    public Order fetchOrderByOrderId(@PathVariable("orderId") long orderId) {
        return this.orderService.fetchOrderById(orderId);
    }

    @PutMapping("/{id}")
    public Order updateOrderById(@PathVariable("id") long orderId, @RequestBody Order order) {
        return this.orderService.updateOrder(orderId, order);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(NO_CONTENT)
    @Operation(method = "Deleting the order based on the order id", responses = {
            @ApiResponse(responseCode = "204", description = "deleting the ordermatching the order id passed")
    })
    public void deleteOrderById(@PathVariable("id") long id) {
        this.orderService.deleteOrderById(id);
    }
}