package com.classpath.ordersapi.event;

public enum EventType {

    ORDER_PENDING,

    ORDER_ACCEPTED,

    ORDER_CANCELLED,

    ORDER_REJECTED
}
