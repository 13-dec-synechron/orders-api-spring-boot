package com.classpath.ordersapi.event;

import com.classpath.ordersapi.model.Order;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OrderEvent {

    private Order order;

    private EventType eventType;

    private LocalDateTime timestamp;
}