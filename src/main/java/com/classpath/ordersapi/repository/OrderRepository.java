package com.classpath.ordersapi.repository;

import com.classpath.ordersapi.model.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;


@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {

    Set<Order> findOrdersByDateBefore(LocalDate localDate);

    Optional<Order> findByCustomer_Email(String emailAddress);

    Set<Order> findByDateBetween(LocalDate startDate, LocalDate endDate);

    Set<Order> findByPriceLessThan(double price);

    Set<Order> findByPriceBetween(double startPrice, double endPrice);
}