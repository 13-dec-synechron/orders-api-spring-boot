package com.classpath.ordersapi.model;

import com.classpath.ordersapi.constraint.ContactNumber;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.sound.sampled.Line;
import javax.swing.text.DateFormatter;
import javax.validation.Valid;
import javax.validation.constraints.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "orders")
@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
public class Order {

    @Id
    @Column(name = "cust_id")
    private Long orderId;

    @JsonProperty(value = "customer", required = true)
    @OneToOne
    @MapsId
    @JoinColumn(name = "cust_id")
    @Valid
    private Customer customer;

    @JsonProperty(value = "price", required = true)
    @Min(value = 15000, message = "order value cannot be less than 15000")
    @Max(value = 45000, message = "order value cannot exceed more than 45000")
    private double price;

    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<LineItem> lineItems = new HashSet<>();

    @JsonSerialize(using = LocalDateSerializer.class)
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonProperty(value = "date", required = true)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    @PastOrPresent(message = "order date cannot be in the future")
    private LocalDate date;

    public void setLineItems(Set<LineItem> lineItems) {
        lineItems.forEach(lineItem -> lineItem.setOrder(this));
        this.lineItems = lineItems;
    }

    //scaffolding code
    //setting the link for both sides of the relationship
    public void addLineItem(LineItem lineItem) {
        if (this.lineItems == null) {
            this.lineItems = new HashSet<>();
        }
        this.lineItems.add(lineItem);
        lineItem.setOrder(this);
    }
}