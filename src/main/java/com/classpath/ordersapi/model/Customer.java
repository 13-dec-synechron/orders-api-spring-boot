package com.classpath.ordersapi.model;

import com.classpath.ordersapi.constraint.ContactNumber;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.validation.annotation.Validated;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Entity
@Table(name = "customer")
@Data
@EqualsAndHashCode(exclude = "order")
@ToString(exclude = "order")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Validated
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotEmpty(message = "customer name cannot be empty")
    private String name;

    @Email(message = "not a valid email address")
    private String email;

    @ContactNumber(message = "contact number cannot be less than 10 and greater the 12")
    @NotEmpty(message = "contact number cannot be empty")
    private String phoneNumber;

    @JsonIgnore
    @OneToOne(mappedBy = "customer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @PrimaryKeyJoinColumn
    private Order order;
}