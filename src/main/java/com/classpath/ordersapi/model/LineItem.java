package com.classpath.ordersapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "line_items")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@EqualsAndHashCode(exclude = "order")
@ToString(exclude = "order")
public class LineItem {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int itemId;

    private String name;

    private int qty;

    private double price;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "order_id", nullable = false)
    private Order order;
}