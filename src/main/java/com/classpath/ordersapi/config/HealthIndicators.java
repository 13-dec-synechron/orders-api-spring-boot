package com.classpath.ordersapi.config;

import com.classpath.ordersapi.repository.OrderRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
class DBHealthIndicator implements HealthIndicator {

    private final OrderRepository orderRepository;

    @Override
    public Health health() {
        return orderRepository.count() >= 0 ?
                Health.up().withDetail("DB-status", "UP").build() : Health.down().withDetail("DB-status", "DOWN").build();
    }
}

@Component
@RequiredArgsConstructor
class KafkaHealthIndicator implements HealthIndicator {
    @Override
    public Health health() {
        return Health.up().withDetail("Kafka-status", "UP").build();
    }
}