package com.classpath.ordersapi.config;

import com.classpath.ordersapi.model.Customer;
import com.classpath.ordersapi.model.LineItem;
import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.CustomerRepository;
import com.classpath.ordersapi.repository.OrderRepository;
import com.github.javafaker.Faker;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;

import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import static java.util.stream.IntStream.range;

@Component
@RequiredArgsConstructor
public class BootstrapAppData implements ApplicationListener<ApplicationReadyEvent> {
//public class BootstrapAppData {

    private final OrderRepository orderRepository;
    private final CustomerRepository customerRepository;
    private final Faker faker = new Faker();


    // @EventListener()
    public void onApplicationEvent(ApplicationReadyEvent event) {
/*
        for(int index = 0; index < 100; index ++){
            Order order = new Order(
                        faker.name().fullName(),
                        faker.number().randomDouble(2, 10_000, 25_000),
                        faker.date().past(5, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            this.orderRepository.save(order);
        }
*/
        Customer customer = Customer
                .builder()
                .name(faker.name().fullName())
                .email(faker.name().firstName() + "@gmail.com")
                .phoneNumber("+919632502541")
                .build();
        Order order = Order
                .builder()
                .customer(customer)
                .price(faker.number().randomDouble(2, 18_000, 25_000))
                .date(faker.date().past(5, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .build();
        customer.setOrder(order);
        range(0, 4).forEach(value -> {
            LineItem lineItem = LineItem
                    .builder()
                    .qty(faker.number().numberBetween(2, 5))
                    .name(faker.commerce().productName())
                    .price(faker.number().randomDouble(2, 25000, 30000))
                    .build();

            order.addLineItem(lineItem);
        });
        this.customerRepository.save(customer);
    }
}