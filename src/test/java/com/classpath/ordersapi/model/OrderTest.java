package com.classpath.ordersapi.model;


import com.github.javafaker.Faker;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.*;

public class OrderTest {
/*    private final Faker faker = new Faker();
    @Test
    public void testConstructor(){
        Order order =  Order
                .builder()
                .customerName(faker.name().fullName())
                .price(faker.number().randomDouble(2, 10_000, 25_000))
                .date(faker.date().past(5, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .build();


        //assertions
        assertNotNull(order);
    }

    @Test
    public void testSetterMethods(){
        Order order =  Order
                .builder()
                .customerName(Vinay)
                .price(faker.number().randomDouble(2, 10_000, 25_000))
                .date(faker.date().past(5, TimeUnit.DAYS).toInstant().atZone(ZoneId.systemDefault()).toLocalDate())
                .build();

        Order order = new Order("Vinay", 22000, LocalDate.of(2021, 10, 10));
        order.setCustomerName("Vinay Kumar");
        order.setPrice(25000);
        order.setDate(LocalDate.of(2021, 9,9));

        //assertions
        assertNotNull(order);
        assertEquals(25000, order.getPrice());
        assertEquals("Vinay Kumar", order.getCustomerName());
    }

    @Test
    public void testEquals(){
        Order order1 = new Order("Vinay", 22000, LocalDate.of(2021, 10, 10));
        Order order2 = new Order("Vinay", 22000, LocalDate.of(2021, 10, 10));

        assertTrue(order1.equals(order2));
    }

    @Test
    public void testNegativeEquals(){
        Order order1 = new Order("Vinay-Kumar", 22000, LocalDate.of(2021, 10, 10));
        Order order2 = new Order("Vinay", 22000, LocalDate.of(2021, 10, 10));

        assertFalse(order1.equals(order2));
    }
    @Test
    public void testHashcode(){
        Order order1 = new Order("Vinay", 22000, LocalDate.of(2021, 10, 10));
        Order order2 = new Order("Vinay", 22000, LocalDate.of(2021, 10, 10));

        assertEquals(order1.hashCode(), order2.hashCode());
    }

    @Test
    public void testNegativeHashcode(){
        Order order1 = new Order("Vinay-Kumar", 22000, LocalDate.of(2021, 10, 10));
        Order order2 = new Order("Vinay", 22000, LocalDate.of(2021, 10, 10));

        assertNotEquals(order1.hashCode(), order2.hashCode());
    }*/
}