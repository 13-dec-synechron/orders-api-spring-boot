package com.classpath.ordersapi.service;

import com.classpath.ordersapi.model.Order;
import com.classpath.ordersapi.repository.OrderRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import javax.annotation.security.RunAs;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

//@ExtendWith(MockitoExtension.class)
public class OrderSeriveTests {
/*
    @Mock
    private OrderRepository orderRepository;

    @InjectMocks
    private OrderService orderService;

    @Test
    public void testSaveOrder(){

        // 1. set the expectations on the mock object
        when(orderRepository.save(any(Order.class)))
                .thenReturn(new Order("Hari", 25000, LocalDate.now()));

        //2. execute
        Order inputOrder = new Order("Hari", 25000, LocalDate.now());

        final Order savedOrder = this.orderService.saveOrder(inputOrder);

        //3. Assert the behavior
        assertNotNull(savedOrder);
        assertEquals("Hari", savedOrder.getCustomerName());
        assertEquals(25000, savedOrder.getPrice());

        //4. verify the mock behavior
        Mockito.verify(orderRepository, times(1)).save(any(Order.class));
    }

    @Test
    public void testFetchAllOrders(){
        when(this.orderRepository.findAll(any(PageRequest.class))).thenReturn(new Page<Order>() {
            @Override
            public int getTotalPages() {
                return 10;
            }

            @Override
            public long getTotalElements() {
                return 100;
            }

            @Override
            public <U> Page<U> map(Function<? super Order, ? extends U> converter) {
                return null;
            }

            @Override
            public int getNumber() {
                return 10;
            }

            @Override
            public int getSize() {
                return 10;
            }

            @Override
            public int getNumberOfElements() {
                return 10;
            }

            @Override
            public List<Order> getContent() {
                return Arrays.asList(new Order("Ram", 10000, LocalDate.of(2021, 10,10)));
            }

            @Override
            public boolean hasContent() {
                return true;
            }

            @Override
            public Sort getSort() {
                return null;
            }

            @Override
            public boolean isFirst() {
                return false;
            }

            @Override
            public boolean isLast() {
                return false;
            }

            @Override
            public boolean hasNext() {
                return false;
            }

            @Override
            public boolean hasPrevious() {
                return false;
            }

            @Override
            public Pageable nextPageable() {
                return null;
            }

            @Override
            public Pageable previousPageable() {
                return null;
            }

            @Override
            public Iterator<Order> iterator() {
                return null;
            }
        });

        final Map<String, Object> response = this.orderService.fetchAllOrders(1, 10, "name");

        Assertions.assertNotNull(response);
        assertNotNull(response.get("data"));
        assertEquals(1, ((List<Order>)response.get("data")).size());
    }

    @Test
    public void testFetchOrderByOrderIdNegative(){
        when(this.orderRepository.findById(anyLong())).thenReturn(Optional.empty());

        try {
            this.orderService.fetchOrderById(12L);
            fail("This test case should throw IllegalArgumentException");
        }catch (IllegalArgumentException exception){
            Assertions.assertNotNull(exception);
        }

        verify(this.orderRepository, times(1)).findById(12L);
    }

    @Test
    public void testFetchOrderByOrderIdNegativeFunctional(){
        when(this.orderRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(IllegalArgumentException.class, () -> this.orderService.fetchOrderById(12));

        verify(this.orderRepository, times(1)).findById(12L);
    }

    @Test
    public void testFetchOrderByValidOrderId(){
        when(this.orderRepository.findById(anyLong())).thenReturn(Optional.of(new Order("Ravi", 10000, LocalDate.now())));
        try {
            final Order order = this.orderService.fetchOrderById(12L);
            Assertions.assertNotNull(order);
            assertEquals("Ravi", order.getCustomerName());
        }catch (IllegalArgumentException exception){
            fail("This test case should throw IllegalArgumentException");
        }
        verify(this.orderRepository, times(1)).findById(12L);
    }

    @Test
    public void testFetchOrderByOrderIdValidFunctional(){
        when(this.orderRepository.findById(anyLong())).thenReturn(Optional.of(new Order("Ravi", 10000, LocalDate.now())));

        assertDoesNotThrow( () -> this.orderService.fetchOrderById(12L));

        verify(this.orderRepository, times(1)).findById(12L);
    }

    @Test
    public void testDeleteOrderByOderId(){
        doNothing().when(this.orderRepository).deleteById(anyLong());
        this.orderService.deleteOrderById(12L);
        verify(this.orderRepository, times(1)).deleteById(12L);
    }*/
}